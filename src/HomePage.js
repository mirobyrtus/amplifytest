import React, { useEffect, useState } from 'react'

import { Auth } from 'aws-amplify'
import { Row, Col, Button, Spinner, Form } from 'react-bootstrap'
import { Redirect } from "react-router-dom"

function HomePage(props) {

  return (
    <div>
      <h3>Welcome {props.user.email}</h3>
    </div>
  )
}

export default HomePage