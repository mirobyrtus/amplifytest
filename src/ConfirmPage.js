import React, { useEffect, useState } from 'react'

import { Auth } from 'aws-amplify'
import { Row, Col, Button, Spinner, Form, Alert } from 'react-bootstrap'
import { Link, withRouter } from "react-router-dom"

function ConfirmPage(props) {

  const [isLoading, setIsLoading] = useState(false)
  const [formConfirmationCode, setFormConfirmationCode] = useState("")
  const [errorMessage, setErrorMessage] = useState("")
  const [infoMessage, setInfoMessage] = useState("")

  let email = props.match.params.email

  async function confirmSignUp() {
    setIsLoading(true)

    Auth.confirmSignUp(email, formConfirmationCode)
      .then((result) => console.log("Confirmation result = " + result))
      .catch((error) => setErrorMessage(error.message || error.log))
      .finally(() => setIsLoading(false))
  }

  const handleConfirmSubmit = (event) => {
    event.preventDefault()

    if (!isLoading) {
      confirmSignUp()
    }
  }

  async function resendConfirmationCode() {
    Auth.resendSignUp(email)
      .then((result) => setInfoMessage('Your verification code is in your inbox'))
      .catch((error) => setErrorMessage(error.message || error.log))
  }

  return (
    <div className="container-fluid p-4 bg-light">
      <Row>
        <Col sm={6} className="m-auto p-4 border bg-white">
          <h4>Confirm</h4>
          {errorMessage && <Alert variant="danger">{errorMessage}</Alert>}
          {infoMessage && <Alert variant="secondary">{infoMessage}</Alert>}
          <Form onSubmit={handleConfirmSubmit}>

            <Form.Group controlId="formBasicConfirmation">
              <Form.Label>Code</Form.Label>
              <Form.Control type="text" placeholder="Confirmation code" required
                onChange={(event) => setFormConfirmationCode(event.target.value)} />
            </Form.Group>

            {/* https://docs.amplify.aws/lib/auth/emailpassword/q/platform/js#custom-attributes */}
            <Button variant="link"
              disabled={isLoading} onClick={() => resendConfirmationCode()}
            >Resend confirmation code</Button>

            {isLoading && <Spinner animation="border" className="d-flex" variant="primary" size="sm"><span className="sr-only">Loading...</span></Spinner>}

            <Button variant="primary" className="w-100 mt-2"
              disabled={isLoading}
              type="submit">Confirm</Button>

          </Form>
        </Col>
      </Row>
    </div>
  )
}

export default withRouter(ConfirmPage)