import React, { useState } from 'react'

import { Auth } from 'aws-amplify'
import { Row, Col, Button, Spinner, Form, Alert } from 'react-bootstrap'
import { Link } from "react-router-dom"

function LoginPage(props) {

  const [isLoading, setIsLoading] = useState(false)
  const [formEmail, setFormEmail] = useState("")
  const [formPassword, setFormPassword] = useState("")
  const [errorMessage, setErrorMessage] = useState("")

  async function signIn() {
    setIsLoading(true)

    Auth.signIn(formEmail, formPassword)
    .then((user) => props.setUser({ email: user.attributes.email }))
    .catch((error) => setErrorMessage(error.message || error.log))
    .finally(() => setIsLoading(false))
  }

  const handleSubmit = (event) => {
    event.preventDefault() // Validate for example email format

    if (!isLoading) {
      signIn()
    }
  }

  return (
    <div className="container-fluid p-4 bg-light">
      <Row>
          <Col sm={6} className="m-auto p-4 border bg-white">
          <h4>Login</h4>
          {errorMessage && <Alert variant="danger">{ errorMessage }</Alert>}
          <Form onSubmit={handleSubmit}>
            
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" required
                            defaultValue={formEmail}
                            onChange={(event) => setFormEmail(event.target.value)} />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder="Password" required
                            onChange={(event) => setFormPassword(event.target.value)} />
            </Form.Group>

            <Link to="/register">Don't have an account yet?</Link>
            
            {isLoading && <Spinner animation="border" className="d-flex" center variant="primary" size="sm"><span className="sr-only">Loading...</span></Spinner>}
            
            <Button variant="primary" className="w-100 mt-2"
                    disabled={isLoading}
                    type="submit">Login</Button>
          </Form>
        </Col>
      </Row>
    </div>
  )
}

export default LoginPage